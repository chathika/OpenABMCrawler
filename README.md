@author: Chathika Gunaratne <chathikagunaratne@gmail.com>
Requirements:
Python 3, pip and Scrapy
To install dependencies (Scrapy https://doc.scrapy.org/)

pip install scrapy
pip install pypiwin32

To run:
scrapy runspider OpenABMCrawler.py