import scrapy
from items import DownloadedFiles
import time
from scrapy.http import Request
import os
import string
class OpenABMSpider(scrapy.Spider):
		name = 'OpenABMSpider'
		
		start_urls = ['https://www.openabm.org/models']
		custom_settings = {
			'ITEM_PIPELINES' : {'scrapy.pipelines.files.FilesPipeline':1},
			'FILES_STORE' : 'C:\Users\ch328575\Documents\Projects\ODDs',
			'DOWNLOAD_FAIL_ON_DATALOSS' : False,
		} 
		current_model = ""
		current_cite = ""
		current_platform = ""
		current_submitted = ""
		current_updated = ""
		def start_requests(self):
			for i in range(1, 29):
				self.start_urls.append('https://www.openabm.org/models?page=' + str(i) );
			for url in self.start_urls:
				yield scrapy.Request(url=url, callback=self.parse)
		def parse(self, response):			
			#time.sleep(0.2)	
			#Go into models
			model_links = response.xpath('//a[contains(@href, "/model/" )]')
			for index, model_link in enumerate(model_links):
				full_model_link = response.urljoin(model_link.xpath('@href').extract()[0])
				print(full_model_link)
				yield scrapy.Request(full_model_link, callback=self.parseModelPage)
				
			
			
			#To go to next page
			#links = response.xpath('//a[contains(@title, "next" )]')
			#next_page = links[0].xpath('@href').extract()
			#next_page = response.urljoin(next_page[0])
			#print(next_page)		
			#if next_page is not None:
				#return scrapy.Request(next_page, callback=self.parse)

		def parseModelPage(self, response):
			self.current_model = response.xpath('//h1/text()').extract()[0]
			self.current_cite = response.xpath('//div[contains(@class, "model-citation-text")]/text()').extract()[0]
			self.current_platform = response.xpath('//div[contains(@class, "model-version-block")]')[0].xpath('//span[contains(@class, "model-block-text")]/text()').extract()[0]
			self.current_submitted = response.xpath('//div[@class="model-block"]').xpath('//div[contains(@class, "model-date")]/text()').extract()[1]
			self.current_updated = response.xpath('//div[@class="model-block"]').xpath('//div[contains(@class, "model-date")]/text()').extract()[2]
			
			with open("models.txt", 'a') as f:
				f.write(self.current_model.encode('utf8'))
				f.write(';')
				f.write(self.current_platform.encode('utf8'))
				f.write(';')
				f.write(self.current_submitted.encode('utf8'))
				f.write(';')
				f.write(self.current_updated.encode('utf8'))
				f.write(';\'')
				f.write(self.current_cite.encode('utf8'))
				f.write('\'\n')
			model_descriptions = response.xpath('//a[contains(@type, "application")]')
			print("FOUND DOCUMENTATION: ")
			for index, model_description in enumerate(model_descriptions):
				print(model_description.xpath('@href').extract()[0])
				yield Request(model_description.xpath('@href').extract()[0], callback=self.save_pdf)
				
		def save_pdf(self, response):
			#Fix String for File Name
			file_path = response.url.split('/')[6] 
			valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
			file_path = ''.join(c for c in file_path if c in valid_chars)
			if not os.path.exists( 'ODDs/' +file_path):
				os.makedirs('ODDs/' +file_path)
			file_name = response.url.split('/')[-1] 
			file_name = file_name[:-11]
			file_name = str(time.time()) + "_"+ file_name + ""
			#Fix String for File Name
			file_name = ''.join(c for c in file_name if c in valid_chars)
			
			print(file_name)
			path = "ODDs/"+ file_path +"/"+ file_name + " "
			self.logger.info('Saving PDF %s', path)
			with open(path, 'wb') as f:
				f.write(response.body)
